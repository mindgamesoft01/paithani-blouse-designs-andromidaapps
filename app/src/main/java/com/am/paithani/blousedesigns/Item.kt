package com.am.paithani.blousedesigns

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
